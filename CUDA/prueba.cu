#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define N (204800*2048)
#define M 512
#define BLOCK_SIZE M
#define RADIUS 3

__global__ void stencil_1d(int *in, int *out) {
	__shared__ int temp[BLOCK_SIZE + 2 * RADIUS];
	int gindex = threadIdx.x + blockIdx.x * blockDim.x;
	int lindex = threadIdx.x + RADIUS;
	// Read input elements into shared memory
	temp[lindex] = in[gindex];
	if (threadIdx.x < RADIUS) {
		temp[lindex - RADIUS] = in[gindex - RADIUS];
		temp[lindex + BLOCK_SIZE] = in[gindex + BLOCK_SIZE];
	}
	// Apply the stencil
	__syncthreads();

	int result = 0;
	for (int offset = -RADIUS ; offset <= RADIUS ; offset++)
		result += temp[lindex + offset];
	// Store the result
	out[gindex] = result;
}

void random_ints(int* a, int n);
void print_vector(int* v, int n);

int main(void)
{
	srand (time(NULL));
	int *in, *out;// host copies of a, b, c
	int *d_in, *d_out;	// device copies of a, b, c
	int size = N * sizeof(int);
	
	// Allocate space for device copies of a, b, c
	cudaMalloc((void **)&d_in, size);
	cudaMalloc((void **)&d_out, size);

	// Alloc space for host copies of a, b, c and setup input values
	in = (int *)malloc(size); random_ints(in, N);
	out = (int *)malloc(size);



	// Copy inputs to device
	cudaMemcpy(d_in, in, size, cudaMemcpyHostToDevice);
	cudaMemcpy(d_out, out, size, cudaMemcpyHostToDevice);

	// Launch add() kernel on GPU
	//add<<<N,1>>>(d_a, d_b, d_c);
	//add<<<1,N>>>(d_a, d_b, d_c);
	clock_t begin, end;
    double time_spent;
    begin = clock();

	stencil_1d<<<N/M,M>>>(d_in, d_out);
	
	end = clock();
    time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    printf("time:%f \n", time_spent) ;

	// Copy result back to host
	cudaMemcpy(out, d_out, size, cudaMemcpyDeviceToHost);

	// Cleanup
	
	cudaFree(d_in); cudaFree(d_out); 

	/*print_vector(in,N);
	print_vector(out,N);*/

	free(in); free(out);
	return 0;
}


void random_ints(int* a, int n)
{
   	int i;
   	for (i = 0; i < n; ++i)
   	{
   		a[i] = rand()%100;
   	}   
}

void print_vector(int* v, int n)
{
	int i;
   	for (i = 0; i < n; ++i)
   	{
   		printf("%d\t", v[i]);
   	}
   	printf("\n");
}