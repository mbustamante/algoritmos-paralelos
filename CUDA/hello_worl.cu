#include <stdio.h>
#include <stdlib.h>
#include <time.h>
//#define N (100000000)
#define N (1994200)
#define M 1

__global__ void add(int *a, int *b, int *c, int n) {
	//*c = *a + *b;
	//c[blockIdx.x] = a[blockIdx.x] + b[blockIdx.x];
	//c[threadIdx.x] = a[threadIdx.x] + b[threadIdx.x];
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	if (index < n)
	{
		c[index] = a[index] + b[index];
	}
}

void random_ints(int* a, int n);
void print_vector(int* v, int n);

int main(void)
{
	srand (time(NULL));
	int *a, *b, *c;	// host copies of a, b, c
	int *d_a, *d_b, *d_c;	// device copies of a, b, c
	int size = N * sizeof(int);
	
	// Allocate space for device copies of a, b, c
	cudaMalloc((void **)&d_a, size);
	cudaMalloc((void **)&d_b, size);
	cudaMalloc((void **)&d_c, size);

	// Alloc space for host copies of a, b, c and setup input values
	a = (int *)malloc(size); random_ints(a, N);
	b = (int *)malloc(size); random_ints(b, N);
	c = (int *)malloc(size);



	// Copy inputs to device
	cudaMemcpy(d_a, a, size, cudaMemcpyHostToDevice);
	cudaMemcpy(d_b, b, size, cudaMemcpyHostToDevice);

	// Launch add() kernel on GPU
	//add<<<N,1>>>(d_a, d_b, d_c);
	//add<<<1,N>>>(d_a, d_b, d_c);
	clock_t begin, end;
    double time_spent;
    begin = clock();
	add<<<(N + M-1)/M,M>>>(d_a, d_b, d_c, N);
	end = clock();
    time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    printf("time:%f \n", time_spent) ;

	// Copy result back to host
	cudaMemcpy(c, d_c, size, cudaMemcpyDeviceToHost);

	// Cleanup
	
	cudaFree(d_a); cudaFree(d_b); cudaFree(d_c);

	/*print_vector(a,N);
	print_vector(b,N);*/
	print_vector(c,N);
	free(a); free(b); free(c);
	return 0;
}


void random_ints(int* a, int n)
{
   	int i;
   	for (i = 0; i < n; ++i)
   	{
   		a[i] = rand()%100;
   	}   
}

void print_vector(int* v, int n)
{
	int i;
   	for (i = 0; i < n; ++i)
   	{
   		printf("%d\t", v[i]);
   	}
   	printf("\n");
}