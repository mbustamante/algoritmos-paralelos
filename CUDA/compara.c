#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define N (100480*2048)
#define THREADS_PER_BLOCK 512



void random_ints(int* a, int n);
void print_vector(int* v, int n);
int main(void)
{
	srand (time(NULL));
	int *a, *b, *c;	// host copies of a, b, c
	long size = N * sizeof(int);

	// Alloc space for host copies of a, b, c and setup input values
	a = (int *)malloc(size); random_ints(a, N);
	b = (int *)malloc(size); random_ints(b, N);
	c = (int *)malloc(size);

	clock_t begin, end;
    double time_spent;
    begin = clock();
	long i;
	for(i =0 ; i<N ; ++i)
	{
		c[i] = a[i] + b[i];
	}
	end = clock();
    time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    printf("time:%f \n", time_spent) ;

	/*print_vector(a,N);
	print_vector(b,N);
	print_vector(c,N);*/
	free(a); free(b); free(c);
	return 0;
}


void random_ints(int* a, int n)
{
   	int i;
   	for (i = 0; i < n; ++i)
   	{
   		a[i] = rand()%100;
   	}   
}

void print_vector(int* v, int n)
{
	int i;
   	for (i = 0; i < n; ++i)
   	{
   		printf("%d\t", v[i]);
   	}
   	printf("\n");
}