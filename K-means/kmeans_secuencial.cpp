#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#define N_CLUSTERS 6
#define N_ELEMENTS 19942
#define N_DIMENSIONES 4745
#define cuadrado(x) ((x)*(x))
#define DIR_DOCUMENTS "space_model/vectors"
#define DIR_CENTROIDS "space_model/centroides"
#define DIR_NAMES "space_model/names"
#define DIR_RESULT "resultados_secuencial/"

using namespace std;

struct centroide
{
	double* centroid;
	int size;
	int id;
};

struct doc
{
	double* vec;
	centroide* centroid;
};


void read_documents(string arc, long dim, long n_documents, doc* &documents)
{
	ifstream myfile (arc.c_str());
	if (!myfile.is_open())
  	{
  		cout << "Archivo no encontrado"; 
  		return;
	}

	documents = new doc[n_documents];
	for(long i = 0 ; i<n_documents; i++)
	{
		documents[i].vec = new double[dim];
		for(long j = 0 ; j<dim; j++)
		{
			myfile >> documents[i].vec[j];
		}
		cout<<"cargando documento: "<<i+1<<endl;

	}
	cout <<"documentos cargados\n";
	myfile.close();
}

void read_centroids(string arc, long dim, long n_clusters, centroide* &centroids)
{
	ifstream myfile (arc.c_str());
	if (!myfile.is_open())
  	{
  		cout << "Archivo no encontrado"; 
  		return;
	}

	centroids = new centroide[n_clusters];
	for(long i = 0 ; i<n_clusters; i++)
	{
		centroids[i].size = 0;
		centroids[i].id = i;
		centroids[i].centroid = new double[dim];
		for(long j = 0 ; j<dim; j++)
		{
			myfile >> centroids[i].centroid[j];
		}
		cout<<"cargando centroide: "<<i+1<<endl;

	}
	cout <<"centroides cargados\n";
	myfile.close();
}

void read_names(string arc, vector<string> &names)
{
	ifstream myfile (arc.c_str());
	if (!myfile.is_open())
  	{
  		cout << "Archivo no encontrado"; 
  		return;
	}

	string buffer;
	int i = 1;
	while(myfile>>buffer)
	{
		names.push_back(buffer);
		cout<<"cargando nombres: "<<i++<<endl;
	}
	cout <<"nombres cargados\n";
	myfile.close();
}

void write_res(string arc , string* &res, int &n_clusters)
{

	for(int i =0 ; i< n_clusters ; i++)
	{
		fstream file;
		string buff = "0123456789";
		string name = arc+buff[i];
		file.open(name.c_str(),fstream::out);
		if (file.is_open())
	  	{
	  		file<<res[i];
			file.close();
		}
		else
		{
			cout << "error al grabar "<<i <<endl; 
		}
	}
}






double cal_distance(double dim, double* point1 , double* point2)
{
	double denominador = 0;
	double mod_p1 = 0;
	double mod_p2 = 0;
	for(int i = 0 ; i<dim ; i++)
	{
		denominador += (point1[i]*point2[i]);
		mod_p1 += cuadrado(point1[i]);
		mod_p2 += cuadrado(point2[i]);
	}
	return denominador/(sqrt(mod_p1)*sqrt(mod_p2));
}

int find_nearest_cluster(int dim, int n_clusters, double* &point, centroide* &c_clusters)
{
	int indice = 0;
	double m_distance = cal_distance(dim, point, c_clusters[0].centroid);
	for(int i = 1 ; i< n_clusters ; i++)
	{
		double tmp_distance = cal_distance(dim, point, c_clusters[i].centroid);
		if(tmp_distance > m_distance)
		{
			indice = i;
			m_distance = tmp_distance;
		}
	}
	return indice;
}



bool calc_centroids( long &n_elements, int &dim, 
					int &n_clusters, centroide* &c_clusters, doc*  &documents)
{
	centroide* old_clusters = new centroide[n_clusters];

	for(int i =0 ; i<n_clusters ; i++)
	{
		old_clusters[i].centroid = (double*) malloc(dim*sizeof(double));
		memcpy( old_clusters[i].centroid, c_clusters[i].centroid, dim*sizeof(double) );
		for(int j = 0 ; j<dim ; j++)
		{
			c_clusters[i].centroid[j] = 0;
		}
	}

	for (long i = 0 ; i<  n_elements ; i++)
	{
		for(int j = 0 ; j<dim ; j++)
		{
			(* documents[i].centroid).centroid[j] += documents[i].vec[j];
		}
	}

	double distance = 0;

	for(int i =0 ; i<n_clusters ; i++)
	{
		for(int j = 0 ; j<dim ; j++)
		{
			c_clusters[i].centroid[j] = c_clusters[i].centroid[j]/c_clusters[i].size;
		}
		c_clusters[i].size = 0;
		distance += cal_distance(dim, old_clusters[i].centroid,c_clusters[i].centroid );
	}

	delete old_clusters;
	printf("ditancia entre centroides: %f\n", distance);

	if(distance == n_clusters)
	{
		return true;
	}
	else
	{
		return false;
	}
}


void calc_clusters(long &n_elements, int &dim, 
					 int &n_clusters, centroide* &c_clusters, doc*  &documents)
{

	for(int i = 0 ; i< n_elements ; i++)
	{
		int indice = find_nearest_cluster(dim, n_clusters, documents[i].vec, c_clusters );
		documents[i].centroid = &(c_clusters[indice]);
		(* documents[i].centroid).size +=1;
	}
}

void k_means( long &n_elements, int &dim, 
					 int &n_clusters, centroide* &c_clusters, doc*  &documents , int n_iteraciones	)
{
	for(int i = 0 ; i< n_iteraciones ; i++)
	{
		printf("iteracion numero: %d\n",i );
		calc_clusters(n_elements, dim,n_clusters,c_clusters,documents);
		for( int j = 0 ; j<n_clusters ; j++)
		{
			printf("cluster %d -> %d documentos\n", j, c_clusters[j].size);
		}
		if(calc_centroids(n_elements,dim,n_clusters,c_clusters,documents))
		{
			printf("\n\n\t\t----CLUSTERING FINALIZADO----\n");
			return;
		}

		printf("\n\n");

	}
	calc_clusters(n_elements, dim,n_clusters,c_clusters,documents);


}




int main()
{
	srand (time(NULL));
	long n_elementos = N_ELEMENTS;
	int dimensiones = N_DIMENSIONES;
	int n_clusters = N_CLUSTERS;
	doc* documentos;
	centroide* c_clusters;
	read_documents(DIR_DOCUMENTS, dimensiones, n_elementos, documentos);
	read_centroids(DIR_CENTROIDS, dimensiones, n_clusters, c_clusters);
	vector<string> names;
	read_names(DIR_NAMES, names);


	clock_t begin, end;
    double time_spent;
    begin = clock();

	k_means(n_elementos, dimensiones, n_clusters, c_clusters, documentos, 50 );


	end = clock();
    time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    printf("time:%f \n", time_spent) ;


	string* clusters = new string[n_clusters];

	for (int i = 0 ; i< n_elementos ; i++)
	{
		clusters[(* documentos[i].centroid).id]+= names[i]+"\n";
	}

	write_res(DIR_RESULT,clusters,n_clusters);

	return 0;
}
