#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define N_CLUSTERS 6
#define N_ELEMENTS 5000
#define N_DIMENSIONES 4745
#define cuadrado(x) ((x)*(x))
#define DIR_DOCUMENTS "space_model/vectors"
#define DIR_CENTROIDS "space_model/centroides"
#define DIR_NAMES "space_model/names"
#define DIR_RESULT "resultados_paralelo/"
#define HILOS 128


using namespace std;

struct centroide
{
	float* centroid;
	int size;
	int id;
};

struct doc
{
	float* vec;
	int id_centroide;
};


void read_documents(string arc, long dim, long n_documents, doc* &documents)
{
	ifstream myfile (arc.c_str());
	if (!myfile.is_open())
  	{
  		cout << "Archivo no encontrado"; 
  		return;
	}

	documents = new doc[n_documents];
	for(long i = 0 ; i<n_documents; i++)
	{
		documents[i].vec = new float[dim];
		documents[i].id_centroide = 0;
		for(long j = 0 ; j<dim; j++)
		{
			myfile >> documents[i].vec[j];
		}
		cout<<"cargando documento: "<<i+1<<endl;

	}
	cout <<"documentos cargados\n";
	myfile.close();
}

void read_centroids(string arc, int dim, int n_clusters, centroide* &centroids)
{
	ifstream myfile (arc.c_str());
	if (!myfile.is_open())
  	{
  		cout << "Archivo no encontrado"; 
  		return;
	}

	centroids = new centroide[n_clusters];
	for(int i = 0 ; i<n_clusters; i++)
	{
		centroids[i].size = 0;
		centroids[i].id = i;
		centroids[i].centroid = new float[dim];
		for(int j = 0 ; j<dim; j++)
		{
			myfile >> centroids[i].centroid[j];
		}
		cout<<"cargando centroide: "<<i+1<<endl;

	}
	cout <<"centroides cargados\n";
	myfile.close();
}

void read_names(string arc, vector<string> &names)
{
	ifstream myfile (arc.c_str());
	if (!myfile.is_open())
  	{
  		cout << "Archivo no encontrado"; 
  		return;
	}

	string buffer;
	int i = 1;
	while(myfile>>buffer)
	{
		names.push_back(buffer);
		//cout<<"cargando nombres: "<<i++<<endl;
	}
	cout <<"nombres cargados\n";
	myfile.close();
}

void write_res(string arc , string* &res, int &n_clusters)
{

	for(int i =0 ; i< n_clusters ; i++)
	{
		fstream file;
		string buff = "0123456789";
		string name = arc+buff[i];
		file.open(name.c_str(),fstream::out);
		if (file.is_open())
	  	{
	  		file<<res[i];
			file.close();
		}
		else
		{
			cout << "error al grabar "<<i <<endl; 
		}
	}
}



__device__ float cal_distance_cuda(float dim, float* point1 , float* point2)
{
	float denominador = 0;
	float mod_p1 = 0;
	float mod_p2 = 0;
	for(int i = 0 ; i<dim ; i++)
	{
		denominador += (point1[i]*point2[i]);
		mod_p1 += cuadrado(point1[i]);
		mod_p2 += cuadrado(point2[i]);
	}
	return denominador/(sqrt(mod_p1)*sqrt(mod_p2));
}


float cal_distance(float dim, float* point1 , float* point2)
{
	float denominador = 0;
	float mod_p1 = 0;
	float mod_p2 = 0;
	for(int i = 0 ; i<dim ; i++)
	{
		denominador += (point1[i]*point2[i]);
		mod_p1 += cuadrado(point1[i]);
		mod_p2 += cuadrado(point2[i]);
	}
	return denominador/(sqrt(mod_p1)*sqrt(mod_p2));
}

__device__  int find_nearest_cluster(int dim, int n_clusters, float* point, float* c_clusters)
{
	int indice = 0;
	float m_distance = cal_distance_cuda(dim, point, c_clusters);
	for(int i = 1 ; i< n_clusters ; i++)
	{
		float tmp_distance = cal_distance_cuda(dim, point, &c_clusters[i*dim]);
		if(tmp_distance > m_distance)
		{
			indice = i;
			m_distance = tmp_distance;
		}
	}
	return indice;
}



bool calc_centroids( int &n_elements, int &dim, 
					int &n_clusters, centroide* &c_clusters, doc*  &documents)
{
	centroide* old_clusters = new centroide[n_clusters];
	cout<<"calculando centroides\n";
	flush(cout);
	for(int i =0 ; i<n_clusters ; i++)
	{
		old_clusters[i].centroid = (float*) malloc(dim*sizeof(float));
		memcpy( old_clusters[i].centroid, c_clusters[i].centroid, dim*sizeof(float) );
		for(int j = 0 ; j<dim ; j++)
		{
			c_clusters[i].centroid[j] = 0;
		}
	}

	for (long i = 0 ; i<  n_elements ; i++)
	{
		for(int j = 0 ; j<dim ; j++)
		{
			c_clusters[documents[i].id_centroide].centroid[j] +=documents[i].vec[j];
		}
	}

	float distance = 0;

	for(int i =0 ; i<n_clusters ; i++)
	{
		for(int j = 0 ; j<dim ; j++)
		{
			c_clusters[i].centroid[j] = c_clusters[i].centroid[j]/c_clusters[i].size;
		}
		c_clusters[i].size = 0;
		distance += cal_distance(dim, old_clusters[i].centroid,c_clusters[i].centroid );
	}

	delete old_clusters;
	printf("ditancia entre centroides: %f\n", distance);

	if(distance == n_clusters)
	{
		return true;
	}
	else
	{
		return false;
	}
}





__global__ void calc_clusters_cuda(int n_elements, int dim, int n_clusters, 
								float* elementos, int*  ids_elementos,
								float* centroides, int* sizes_centroides)
{
	int index = blockIdx.x;

	//printf("indice: %d\n",index);
	int indice = 0;
	float denominador = 0;
	float mod_p1 = 0;
	float mod_p2 = 0;

	for(int i = 0 ; i< dim ; i++)
	{
		//printf("indice: %d -> elemento:  %f - centro: %f\n",index, elementos[index*(*dim) +i],centroides[i]);
		denominador += (elementos[index*dim +i]*centroides[i]);
		mod_p1 += cuadrado(elementos[index*dim +i]);
		mod_p2 += cuadrado(centroides[i]);
	}
	//printf("denominador -> %f\n", denominador);
	float m_distance = denominador/(sqrt(mod_p1)*sqrt(mod_p2));
	//printf("0 -> %f\t", m_distance);

	for(int i = 1 ; i<  n_clusters ; i++)
	{
		denominador = 0;
		mod_p1 = 0;
		mod_p2 = 0;

		for(int j = 0 ; j< dim ; j++)
		{
			denominador += (elementos[index*dim +j]*centroides[i*dim+j]);
			mod_p1 += cuadrado(elementos[index*dim +j]);
			mod_p2 += cuadrado(centroides[i*dim+j]);
		}
		//printf("%d -> %f\t", i,denominador);
		float tmp_distance = denominador/(sqrt(mod_p1)*sqrt(mod_p2));
		//printf("%d -> %d\t", i,tmp_distance);
		if(tmp_distance > m_distance)
		{
			indice = i;
			m_distance = tmp_distance;
		}
	}


	ids_elementos[index] = indice;
	sizes_centroides[indice] +=1;

}

void k_means( int &n_elements, int &dim, 
					 int &n_clusters, centroide* &c_clusters, doc*  &documents , int n_iteraciones	)
{
	
	float* h_elements = (float*) malloc(sizeof(float)*n_elements*dim);	//temporal para guardar direcciones de documentos
	float* d_elements;	//elementos en el device
	
	for(int k = 0 ; k<n_elements ; k++)
	{
		for(int j = 0 ; j< dim ; j++)
		{
			h_elements[k*dim + j] = documents[k].vec[j];
		}
	}
	cudaMalloc((void**) &d_elements, sizeof(float)*n_elements*dim);	//separa memoria en forma unidimensional
	cudaMemcpy(d_elements, h_elements, sizeof(float)*dim*n_elements, cudaMemcpyHostToDevice);
	free(h_elements);

	for(int i = 0 ; i< n_iteraciones ; i++)
	{

		/* copiar ids de los centroides*/
		printf("iteracion numero: %d\n",i );
		int* h_elem_ids = (int*) malloc(n_elements*sizeof(int));
		int* h_elem_ids2 = (int*) malloc(n_elements*sizeof(int));
		int* d_elem_ids;

		for(int k = 0 ; k<n_elements ; k++)
		{
			h_elem_ids[k] = documents[k].id_centroide;			
		}
		cudaMalloc((void**) &d_elem_ids, sizeof(int)*n_elements);	//separa memoria unidimensional
		cudaMemcpy(d_elem_ids, h_elem_ids, sizeof(int)*n_elements, cudaMemcpyHostToDevice);
		flush(cout);
		

		/*copiar data de los centroides y el tamaño de los centroides*/
		float* h_centroids = (float*) malloc(n_clusters*sizeof(float)*dim);
		int* h_sizes = (int*) malloc(n_clusters*sizeof(int));
		int* h_sizes2 = (int*) malloc(n_clusters*sizeof(int));
		float* d_centroids;
		int* d_sizes;

		for(int k = 0 ; k<n_clusters ; k++)
		{
			h_sizes[k] = c_clusters[k].size;
			for(int j = 0 ; j< dim ; j++)
			{
				h_centroids[k*dim + j] = c_clusters[k].centroid[j];

			}
			//cout<< h_sizes[k]<<"\n";
			//flush(cout);	
		}
		cout<<"\n\n\n";

		cudaMalloc((void**) &d_centroids, sizeof(float)*n_clusters*dim);
		cudaMalloc((void**) &d_sizes, sizeof(int)*n_clusters);
		cudaMemcpy(d_centroids, h_centroids, sizeof(float)*dim*n_clusters, cudaMemcpyHostToDevice);
		cudaMemcpy(d_sizes, h_sizes, sizeof(int)*n_clusters, cudaMemcpyHostToDevice);


		cout<<"CUDA\n";
		flush(cout);
		calc_clusters_cuda<<<n_elements,1>>>(n_elements, dim, n_clusters,
																	d_elements, d_elem_ids, d_centroids, d_sizes);


		cudaMemcpy(h_elem_ids2, d_elem_ids, sizeof(int)*n_elements, cudaMemcpyDeviceToHost);
		for(int k = 0 ; k<n_elements ; k++)
		{
			documents[k].id_centroide = h_elem_ids2[k];
		}

		cudaMemcpy(h_sizes2, d_sizes, sizeof(int)*n_clusters, cudaMemcpyDeviceToHost);

		for(int k = 0 ; k<n_clusters ; k++)
		{
			c_clusters[k].size = h_sizes2[k];	
		}

		free(h_elem_ids);
		free(h_elem_ids2);
		free(h_centroids);
		free(h_sizes);
		free(h_sizes2);
		
		cudaFree(d_centroids);
		cudaFree(d_elem_ids);
		cudaFree(d_sizes);
		
		for( int j = 0 ; j<n_clusters ; j++)
		{
			printf("cluster %d -> %d documentos\n", j, c_clusters[j].size);
		}

		if(calc_centroids(n_elements,dim,n_clusters,c_clusters,documents))
		{
			printf("\n\n\t\t----CLUSTERING FINALIZADO----\n");
			return;
		}

		printf("\n\n");

	}
	//calc_clusters<<<(n_elements+HILOS-1)/HILOS,HILOS>>>(n_elements, dim,n_clusters,c_clusters,documents);
	cudaFree(d_elements);
}




int main()
{
	int n_elementos = N_ELEMENTS;
	int dimensiones = N_DIMENSIONES;
	int n_clusters = N_CLUSTERS;
	doc* documentos;
	centroide* c_clusters;
	read_documents(DIR_DOCUMENTS, dimensiones, n_elementos, documentos);
	read_centroids(DIR_CENTROIDS, dimensiones, n_clusters, c_clusters);
	vector<string> names;
	read_names(DIR_NAMES, names);

	k_means(n_elementos, dimensiones, n_clusters, c_clusters, documentos, 50 );


	string* clusters = new string[n_clusters];

	for (int i = 0 ; i< n_elementos ; i++)
	{
		clusters[documentos[i].id_centroide]+= names[i]+"\n";

	}

	write_res(DIR_RESULT,clusters,n_clusters);

	return 0;
}
