# -*- coding:utf-8-*-

from unicodedata import normalize
import os
import re
from nltk.stem.lancaster import LancasterStemmer
import nltk
from nltk.corpus import wordnet as wn
import numpy

stemmer = LancasterStemmer()

f = open('stop_words_english', 'r')
data = f.read().split("\n")
f.close()

stop = data#stopwords.words('english')

def normalize_string(word):
    word = normalize('NFKD', word).encode('utf-8', 'ignore')
    return word

def remove_punctuation_marks(word):
    if re.match("^[a-z\xE1\xE9\xED\xF3\xFA\xF1]+$", word):
        return word
    else:
        new_word = ""
        for i in range(len(word)):
            if re.match("[\w\xE1\xE9\xED\xF3\xFA\xF1]0-9", word[i]):
                new_word += word[i]
        return new_word
    
def clean_string(string):
    try:
        tokens = [token for token in string.lower().split() if token not in stop]
        data_clean = " ".join([stemmer.stem(remove_punctuation_marks(token)) for token in tokens])
        return data_clean
    except:
        print(string)
        print("error ")
        return ''

general_dict={}
documents_dict = {}
n_documents = 0
for root, dirs, files in os.walk("news/"  ):
        #print(files)
        for file in files:
            name_file = file
            try:
                if file.endswith(""):
                    #print(file)
                    n_documents+=1
                    print(n_documents)
                    file_dict= {}
                    ff = (os.path.join(root, file))
                    file = open(ff)
                    text =file
                    p = text.read()
                    p =  p.replace("'", "")
                    p = p.split()
                    text = clean_string(" ".join(p)).split()
                    for word in text:
                        try:
                            file_dict[word] = file_dict[word]+1
                        except:
                            file_dict[word] = 1
                    #print(file_dict)
                    
                    documents_dict[name_file] = file_dict
                    
                    for key in list(file_dict.keys()):
                        try:
                            general_dict[word] = general_dict[word]+1
                        except:
                            general_dict[word] = 1
                            
            except Exception as e:
                print(file)
                
                
                
list_keys = list(general_dict.keys())
list_documents = list(documents_dict.keys())
print("dimensiones : ", len(list_keys))


count  = 0
dir_vectors = 'space_model/vectors'
dir_names = 'space_model/names'
dir_keys = 'space_model/keys'

file = open(dir_keys,'w')
file.write( "\n".join(list_keys))
file.close()

file = open(dir_names,'w')
file.write( "\n".join(list_documents))
file.close()

file = open(dir_vectors,'a')
vectors = []
names = []
for document in list_documents:
    count+=1
    doc_dict =[]
    for key in list_keys:
        if key in documents_dict[document]:
            doc_dict.append(documents_dict[document][key]*numpy.log(n_documents/(general_dict[key])))
        else:
            doc_dict.append(0)
    vectors.append(doc_dict)
    names.append(document)
    file.write( " ".join(map(str, doc_dict)))
    file.write( "\n")
    #print(documents_dict[document])
    #file = open(dir+document,'w')
    #file.write(str(doc_dict))
    
    print(count)
file.close()
            
            

