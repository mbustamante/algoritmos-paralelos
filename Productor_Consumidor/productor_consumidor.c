#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#define TAM_BUFFER 1000
typedef int TIPO_DATO;


typedef struct{
	int cont;
	int* buff;
} queue;


pthread_mutex_t mutex1 = PTHREAD_MUTEX_INITIALIZER;

void* consumir( void *ptr );
void* producir( void *ptr );
void *print_message_function( void *ptr );

int main()
{
	srand(time(0));
	queue buffer;
	buffer.cont =0;
	buffer.buff = (int *) calloc(TAM_BUFFER,sizeof(TIPO_DATO));
	pthread_t productor, consumidor;



	pthread_create( &consumidor, NULL, consumir, (void*) &buffer);
	pthread_create( &productor, NULL,producir , (void*) &buffer);
	

	
	pthread_join( consumidor, NULL);
	pthread_join( productor, NULL);

	exit(0);
}

void* consumir( void* ptr )
{
	printf("consumidor\t");
	queue* buffer = (queue *) ptr;
	while(1)
	{
		if(buffer->cont == 0)	sleep(2);
		pthread_mutex_lock( &mutex1 );
		printf("Consumido: %d\ttamanio: %d\n\n", buffer->buff[--(buffer->cont)],buffer->cont -1);
		buffer->buff[(buffer->cont)] = 0;
		pthread_mutex_unlock( &mutex1 );
		sleep(2);
	}

}


void* producir( void* ptr )
{
	printf("productor\t");
	queue* buffer = (queue *) ptr;
	while(1)
	{
		if(buffer->cont >= TAM_BUFFER)	sleep(2);
		pthread_mutex_lock( &mutex1 );
		buffer->buff[(buffer->cont)++] = rand();
		printf("Producido: %d\ttamanio: %d\n\n", buffer->buff[buffer->cont -1],buffer->cont);
		pthread_mutex_unlock( &mutex1 );
		sleep(1);

	}

}