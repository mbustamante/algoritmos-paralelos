#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#define TAMANIO 5
typedef int TIPO_DATO;

typedef struct{
	int (*fp)( const int, const int);
	int filas;
	int columnas;
	int** data;
} matriz;

typedef struct{
	matriz * mat1;
	matriz * mat2;
} args;



int fun1(const int i,const int j){
	return i+j;
}


int fun2(const int i,const int j){
	return i+2*j;
}

int fun3(const int i,const int j){
	return 2*i+3*j;
}

int fun4(const int i,const int j){
	return 2*i+j;
}


void* llenar_matriz(void* ptr);
void print_matriz(void* ptr);
void* multiplicar(void* ptr);
void* sumar(void* ptr);



int main()
{

	matriz A;	A.filas = TAMANIO;	A.columnas = TAMANIO;	A.fp = fun1;
	matriz B;	B.filas = TAMANIO;	B.columnas = TAMANIO;	B.fp = fun2;
	matriz C;	C.filas = TAMANIO;	C.columnas = TAMANIO;	C.fp = fun3;
	matriz D;	D.filas = TAMANIO;	D.columnas = TAMANIO;	D.fp = fun4;
	pthread_t matriz_A, matriz_B, matriz_C, matriz_D, multiplicacion1, multiplicacion2, suma;

	pthread_create( &matriz_A, NULL, llenar_matriz, (void*) &A);
	pthread_create( &matriz_B, NULL, llenar_matriz, (void*) &B);
	pthread_create( &matriz_C, NULL, llenar_matriz, (void*) &C);
	pthread_create( &matriz_D, NULL, llenar_matriz, (void*) &D);
	pthread_join(matriz_A, NULL);
	pthread_join(matriz_B, NULL);
	args mult1;
	mult1.mat1 = &A;
	mult1.mat2 = &B;
	pthread_create( &multiplicacion1, NULL, multiplicar, (void*) &mult1);
	void *ptr_res_mult1;

	pthread_join(matriz_C, NULL);
	pthread_join(matriz_D, NULL);
	args mult2;
	mult2.mat1 = &C;
	mult2.mat2 = &D;
	pthread_create( &multiplicacion2, NULL, multiplicar, (void*) &mult2);
	void *ptr_res_mult2;

	pthread_join(multiplicacion1, &ptr_res_mult1);
	matriz *m_multi1 = (matriz *) ptr_res_mult1;

	pthread_join(multiplicacion2, &ptr_res_mult2);
	matriz *m_multi2 = (matriz *) ptr_res_mult2;


	args sum;
	sum.mat1 = m_multi1;
	sum.mat2 = m_multi2;
	pthread_create(&suma, NULL, sumar, (void*) &sum);
	void *ptr_res_final;
	pthread_join(suma, &ptr_res_final);
	matriz *res_final = (matriz *) ptr_res_final;

	printf("matriz A\n");
	print_matriz(&A);
	printf("matriz B\n");
	print_matriz(&B);
	printf("matriz C\n");
	print_matriz(&C);
	printf("matriz D\n");
	print_matriz(&D);

	printf("matriz A*B\n");
	print_matriz(m_multi1);
	printf("matriz C*D\n");
	print_matriz(m_multi2);
	printf("matriz A*B+C*D\n");
	print_matriz(res_final);

	exit(0);
}


void* llenar_matriz(void* ptr)
{
	matriz* mat = (matriz *) ptr;
	mat->data = (int **) calloc(mat->filas,sizeof(int*));
	int i,j;
	for( i = 0 ; i< mat->filas ; i++)
	{
		mat->data[i] = (int *) calloc(mat->columnas,sizeof(TIPO_DATO));
		for ( j =0 ; j< mat->columnas ; j++)
		{
			mat->data[i][j] = mat->fp(i,j);
		}

	}
}


void print_matriz(void* ptr)
{
	matriz* mat = (matriz *) ptr;
	int i,j;
	for( i = 0 ; i< mat->filas ; i++)
	{
		for (j =0 ; j< mat->columnas ; j++)
		{
			printf("%d\t", mat->data[i][j] );
		}
		printf("\n\n" );
	}
	printf("\n\n" );
}

void* multiplicar(void* ptr)
{
	//printf("multiplicando\n");
	matriz *mat1 = ((args *) ptr)->mat1;
	matriz *mat2 = ((args *) ptr)->mat2;
	matriz *res = malloc(sizeof(matriz));
	res->filas = mat1->filas;
	res->columnas = mat2-> columnas;
	res->data = (TIPO_DATO **) calloc(res->filas,sizeof(TIPO_DATO*));
	int i,j;
	for( i = 0 ; i< res->filas ; i++)
	{
		res->data[i] = (TIPO_DATO *) calloc(res->columnas,sizeof(TIPO_DATO));
		for ( j =0 ; j< res->columnas ; j++)
		{
			int k, acumulador = 0;
			for(k = 0 ; k< mat1->columnas ; k++)
			{
				acumulador+= mat1->data[i][k] * mat2->data[k][j];
			}
			res->data[i][j] = acumulador;
		}

	}
	pthread_exit(res);
}



void* sumar(void* ptr)
{
	//printf("sumando\n");
	matriz *mat1 = ((args *) ptr)->mat1;
	matriz *mat2 = ((args *) ptr)->mat2;
	matriz *res = malloc(sizeof(matriz));
	res->filas = mat1->filas;
	res->columnas = mat1->columnas;
	res->data = (TIPO_DATO **) calloc(res->filas,sizeof(TIPO_DATO*));
	int i,j;
	for( i = 0 ; i< res->filas ; i++)
	{
		res->data[i] = (TIPO_DATO *) calloc(res->columnas,sizeof(TIPO_DATO));
		for ( j =0 ; j< res->columnas ; j++)
		{
			res->data[i][j] = mat1->data[i][j] + mat2->data[i][j];
		}

	}

	pthread_exit(res);
}