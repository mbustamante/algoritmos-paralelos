#include <mpi.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#define TAM 10000000

void print_vector(int* vector, int tam);
void broadcast(void* data, int count, MPI_Datatype datatype, int root, MPI_Comm communicator);

int main(int argc, char** argv ){
    clock_t begin, end;
    double time_spent;
    begin = clock();

    int i,n_proc, procs, miRank, parte, ind = 0;
    int* vec = malloc(TAM*sizeof(int));
    MPI_Status status;

    MPI_Init(&argc,&argv);
    MPI_Comm_size(MPI_COMM_WORLD, &procs);
    MPI_Comm_rank(MPI_COMM_WORLD, &miRank);

    if(miRank ==0)
    {
        srand(time(NULL));
        for(i = 0 ; i<TAM ; i++)
            vec[i] = rand()%1000;
        //print_vector(vec,TAM);
    }


    broadcast(vec, TAM, MPI_INT, 0,MPI_COMM_WORLD);
    if(miRank ==0)
    {
        printf("MASTER: \n");
        //print_vector(vec,TAM);
        /*for(i = 0 ; i<TAM ; i++)
            vec[i] = 0;*/
    }
    else
    {
        //sleep(1);
        printf("SLAVE: \n");
        //print_vector(vec,TAM);
    }

    MPI_Finalize();
    end = clock();
    time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    printf("%d -> time:%f \n",miRank, time_spent) ;
    return 0;
}

void print_vector(int* vector, int tam)
{
	int i;
	printf("[ ");
	for(i = 0 ; i<tam ; i++)
		printf(" %d ", vector[i]);
	printf(" ]\n");
}

void broadcast(void* data, int count, MPI_Datatype datatype, int root, MPI_Comm communicator)
{
	int miRank, i, procs;;
  	MPI_Comm_rank(communicator, &miRank);
  	MPI_Comm_size(communicator, &procs);
 
  	if (miRank== root)
  	{
  		for (i = 0; i < procs; i++)
  		{
      		if (i != miRank)
      		{
        		MPI_Send(data, count, datatype, i, 0, communicator);
      		}
   		}
  	}
  	else
  	{
    	MPI_Recv(data, count, datatype, root, 0, communicator, MPI_STATUS_IGNORE);
  	}
}