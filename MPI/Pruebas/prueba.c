#include <mpi.h>
#include <stdio.h>

int main(int argc, char** argv ){

	int i, envia,recibe;
	int procs, miRank;
	int tag =10;

	MPI_Status status;
	MPI_Init(&argc,&argv);
	MPI_Comm_size(MPI_COMM_WORLD, &procs);
	MPI_Comm_rank(MPI_COMM_WORLD, &miRank);
	

	if(miRank ==0)
	{
		for (i = 1 ; i<procs ; i++)
		{
			MPI_Recv(&recibe,1, MPI_INT, i, tag, MPI_COMM_WORLD, &status );
			printf("Recibido el valor %d del proceso %d\n", recibe, i);

		}
	}
	else
	{
		envia = miRank *2;
		MPI_Send(&envia, 1, MPI_INT, 0, tag, MPI_COMM_WORLD);
	}


	MPI_Finalize();
	return 0;
}