#include <mpi.h>
#include <stdio.h>
#include <time.h>
#define TAM 10

void print_vector(int* vector, int tam);

int main(int argc, char** argv ){

	int i, procs, miRank, parte, ind = 0;
	int vec[TAM];
	int tag =10;
	MPI_Status status;

	MPI_Init(&argc,&argv);
	MPI_Comm_size(MPI_COMM_WORLD, &procs);
	MPI_Comm_rank(MPI_COMM_WORLD, &miRank);

	parte = (int)(TAM / (procs-1));
	

	if(miRank ==0)
	{
		srand(time(NULL));
		for(i = 0 ; i<TAM ; i++)
			vec[i] = rand()%1000;
		print_vector(vec,TAM);

		for (i = 1 ; i<procs ; i++)
		{
			MPI_Send(&vec[ind],parte, MPI_INT, i, tag, MPI_COMM_WORLD );
			ind = ind +parte;

		}
	}
	else
	{
		MPI_Recv(vec, parte, MPI_INT, 0, tag, MPI_COMM_WORLD, &status);
		printf("Proc. %d recibe [ ",miRank);
		for(i = 0 ; i<parte ; i++)
			printf("%d ", vec[i]);
		printf("] Final de proc. %d\n",miRank);
	}


	MPI_Finalize();
	return 0;
}

void print_vector(int* vector, int tam)
{
	int i;
	printf("[ ");
	for(i = 0 ; i<tam ; i++)
		printf(" %d ", vector[i]);
	printf(" ]\n");
}