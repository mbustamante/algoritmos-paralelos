#include <mpi.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#define TAM 1000

void print_vector(int* vector, int tam);
void quicksort(int vec[],int n);
void fusionar(int* v1 , int *v2 , int tam_v1, int tam_v2, int* res);


int main(int argc, char** argv ){

    int i,n_proc, procs, miRank, parte, ind = 0;
    int vec[TAM];
    //int* vec = malloc(TAM*sizeof(int));
    int tag =10;
    MPI_Status status;

    MPI_Init(&argc,&argv);
    MPI_Comm_size(MPI_COMM_WORLD, &procs);
    MPI_Comm_rank(MPI_COMM_WORLD, &miRank);

    if(miRank ==0)
    {
        srand(time(NULL));
        for(i = 0 ; i<TAM ; i++)
            vec[i] = rand()%1000;
        print_vector(vec,TAM);
    }

    parte = (int)(TAM / (procs-1));
    int buffer[parte];
    //int* buffer = malloc(parte*sizeof(int));
    
    MPI_Scatter(vec, parte, MPI_INT, buffer, parte, MPI_INT, 0, MPI_COMM_WORLD);
    quicksort(buffer,parte);
    MPI_Gather(buffer, parte, MPI_INT, vec, parte, MPI_INT, 0, MPI_COMM_WORLD);

    if(miRank ==0)
    {
        print_vector(vec,TAM);
        for(i = 1 ; i <(procs-1) ; i++)
        {
            int res[parte*(i+1)];
            //int * res = malloc(parte*(i+1)*sizeof(int));
            fusionar(&vec[0],  &vec[parte*i], parte*i, parte ,res);
            int j;
            for(j = 0 ; j< parte*(i+1) ; j++)
                vec[j] = res[j];
            //print_vector(res,parte*(i+1));
        }
        print_vector(vec,TAM);
    }




    MPI_Finalize();
    return 0;
}

void print_vector(int* vector, int tam)
{
    int i;
    printf("[ ");
    for(i = 0 ; i<tam ; i++)
        printf(" %d ", vector[i]);
    printf(" ]\n");
}


void qs(int* vec,int limite_izq,int limite_der)
{
    int izq,der,temporal,pivote;

    izq=limite_izq;
    der = limite_der;
    pivote = vec[(izq+der)/2];

    do{
        while(vec[izq]<pivote && izq<limite_der)izq++;
        while(pivote<vec[der] && der > limite_izq)der--;
        if(izq <=der)
        {
            temporal= vec[izq];
            vec[izq]=vec[der];
            vec[der]=temporal;
            izq++;
            der--;

        }

    }while(izq<=der);
    if(limite_izq<der)  {qs(vec,limite_izq,der);}
    if(limite_der>izq)  {qs(vec,izq,limite_der);}

}

void quicksort(int* vec,int n)
{
    qs(vec,0,n-1);
}



void fusionar(int* v1 , int *v2 , int tam_v1, int tam_v2, int* res)
{
    int t = tam_v1+tam_v2, t1=0, t2=0, i;
    for( i=0 ; i<t ; i++){
        if(t1==tam_v1)
            res[i] = v2[t2++];
        else
            if(t2==tam_v2)
                res[i] = v1[t1++];
            else
                if( v1[t1] < v2[t2] )
                    res[i] = v1[t1++];
                else
                    res[i] = v2[t2++];
    }
}