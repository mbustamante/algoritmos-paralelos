#include <omp.h>
#include <stdio.h>

int main()
{
	int m = 10;
	int n = 3;
	int a[m][n];
	int x[n];
	int y[m];
	int i,j;


	for(i = 0 ; i<m;i++)
	{
		for(j =0 ; j<n ; j++)
		{
			a[i][j] = i+j;
			x[j] = j;
		}
	}

	#pragma omp parallel for
	for( i =0 ; i<m; i++)
	{
		y[i] =0;
		for( j = 0 ; j<n; j++)
		{
			y[i]+=a[i][j]*x[j];
		}
		printf("%d\ttread: %d\n",y[i], omp_get_thread_num());
	}
	return 0;
}
